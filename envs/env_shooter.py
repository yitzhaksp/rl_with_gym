from gym import spaces
import copy
import numpy as np


class state:
    def __init__(self,ball,ball_velocity,target,target_velocity):
        self.ball=ball
        self.ball_velocity=ball_velocity
        self.target=target
        self.target_velocity=target_velocity

    def to_str(self):
        return str(self.ball[0])+'_'+str(self.ball[1])+'_'+str(self.target)


def s_to_str(s):
    if s is None:
        return 'none'
    else:
        return s.to_str()

class shooter_env:
    def __init__(self,width,length,gun,obstacle):
        self.field_length=length
        assert(width>=2)
        self.field_width=width
        assert(gun<width)
        self.gun=gun
        assert(obstacle['top'][1]>0)
        assert(obstacle['top'][0]+obstacle['length']-1<width)
        self.obstacle=obstacle
        self.action_space = spaces.Discrete(4)

    def reset(self):
        self.s=state(np.array([self.gun,0],dtype=int),
                     np.array([0,0],dtype=int),
                     np.random.randint(self.field_width),
                     2*np.random.randint(2)-1
                     )
        return copy.deepcopy(self.s)


    def step(self,a):
        assert(self.s.ball[0] >= 0 and self.s.ball[0] < self.field_width)
        assert(self.s.ball[1] >= 0 and self.s.ball[1] < self.field_length)
        assert(self.s.target >= 0 and self.s.target < self.field_width)
        r=0
        done=False
        if self.s.ball[1]==0 and self.s.ball[0]==self.gun:
            if a==0:
                pass
            elif a==1:
                self.s.ball_velocity=np.array([-1,1],dtype=int)
            elif a == 2:
                self.s.ball_velocity = np.array([0, 1], dtype=int)
            elif a == 3:
                self.s.ball_velocity = np.array([1, 1], dtype=int)
        if self.s.ball[0]==0 or (self.s.ball[0]==self.field_width-1): #ball at wall
            if self.s.ball[0] == 0:
                self.s.ball_velocity[0]=abs(self.s.ball_velocity[0])
            elif self.s.ball[0] ==self.field_width-1:
                self.s.ball_velocity[0] = -abs(self.s.ball_velocity[0])
        self.s.ball+=self.s.ball_velocity
        if self.s.target==0 or (self.s.target==self.field_width-1): #target at wall
            if self.s.target == 0:
                self.s.target_velocity=abs(self.s.target_velocity)
            elif self.s.target == self.field_width-1:
                self.s.target_velocity=-abs(self.s.target_velocity)
        self.s.target+=self.s.target_velocity
        if self.s.ball[1] == self.obstacle['top'][1]: #ball hits column with obstacle
            if self.s.ball[0] >= self.obstacle['top'][0] and self.s.ball[0] < self.obstacle['top'][0]+self.obstacle['length'] :
                done=True
                r=-1
        if self.s.ball[1] == self.field_length-1:
            done=True
            r=1 if self.s.ball[0]==self.s.target else -1
        return copy.deepcopy(self.s),r,done
