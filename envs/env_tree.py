from copy import  *

class node:
    def __init__(self,id,dist,children,r=0,dist_to_bump=[]):
        #self.level=level
        self.id=id
        self.dist=dist #distance to parent node
        self.r=r
        self.children=children
        self.dist_to_bump=dist_to_bump
        for d in dist_to_bump:
            assert d>0
    def __eq__(self,other):
        return self.id==other.id

class state:
    def __init__(self,dist,node,bump=False,damaged=False):
        self.dist=dist #dist to next junction
        self.node=node # next junction
        self.damaged=damaged
        self.bump=bump

    def __eq__(self,other):
        if self.dist == other.dist and self.node == other.node:
            if self.bump==other.bump and self.damaged==other.damaged:
                return True
        else:
            return False

    def to_str(self):
        return self.node.id + '_dist_' + str(self.dist)

class tree_env():
    #node=(level,num,dist_to_parent,children=[(lev)])
    #s=(dist_next_node,next_node)
    def __init__(self,root):
        self.root=root #root is a node

    def set_state(self,s):
        #assert s.node in self.tree
        self.s=s

    def reset(self):
        self.s=state(0,self.root)
        return self.s

    def add_damaged_states(self,states):
        states_out=[]
        for s in states:
            states_out.append(s)
            s_dmg=copy.copy(s)
            s_dmg.damaged=True
            states_out.append(s_dmg)
        return states_out


    def get_actions(self,s):
        if s.dist>0:
            actions=[state(s.dist-1,s.node)]
        else:
            actions=[state(child.dist-1,child) for child in s.node.children]
        if s.bump:
            actions=self.add_damaged_states(actions)
        for a in actions:
            if a.dist in a.node.dist_to_bump:
                a.bump=True
        return actions

    def step(self,a):
        assert a in self.get_actions(self.s)
        if a.dist>0:
            r=0
        else:
            r=a.node.r
        if self.s.bump and a.damaged:
            r-=2
        self.s = a
        done=True if (not self.get_actions(self.s)) else False
        return self.s,r,done

