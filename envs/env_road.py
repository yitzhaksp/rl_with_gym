import copy
import numpy as np
###################################
# bumpy road environment
###################################

action_names=['n', 'c']

class state:
    def __init__(self,pos,bump=False):
        self.pos=pos
        self.bump=bump

    def __eq__(self,other):
        pass

    def to_feat(self):
        return np.array([self.pos,int(self.bump)])

class action:
    def __init__(self,name):
        assert name in action_names
        self.name=name

    def to_feat(self):
        y= 2-action_names.index(self.name)
        return np.array([y])


class road_env:
    def __init__(self,length,bumps):
        self.length=length
        self.bumps=bumps
        self.actions=[action(name) for name in action_names ]
        self.term_val=0

    def set_state(self,pos):
        assert pos <=self.length
        bump= (pos in self.bumps)
        self.s=state(pos,bump=bump)

    def get_actions(self,s):
        return self.actions

    def step(self,a):
        assert a in self.get_actions(self.s)
        if not self.s.bump:
            r=-1 if a.name=='n' else -2
        else:
            r=-3 if a.name=='n' else -2
        self.s.pos+=1
        self.s.bump = True if (self.s.pos in self.bumps) else False
        done=True if self.s.pos==self.length else False
        return copy.deepcopy(self.s),r,done

    def terminal(self,s):
        return s.pos >=self.length


#to string
def s_to(s):
    # chld_str=''
    # for child in s[1].children:
    #     chld_str+='_'+child.id
    #return str(s[0])+'_lev_'+s[1].id+'_chld'+chld_str
    if isinstance(s,str):
        return s
    else:
        bmp='bmp' if s.bump else 'flt'
        return bmp+str(s.pos)


#to string
def a_to(a):
    return a.name

def c_to(s):
    c_str='bmp' if s.bump else 'flt'
    return c_str

def h(s):
    if s.dist==0:
        c=1
    else:
        c=0
    return c



