from envs.env_shooter import *
from utils.RL_brain_crit import *
from collections import deque
import time
import numpy as np
import os

width,length,gun=10,20,2
obstacle={'top':[2,8] ,'length':3 }
env=shooter_env(width,length,gun,obstacle)
def get_actions(s):
    return range(env.action_space.n)
def x_to_str(x):
    return 'prev_'+ s_to_str(x[0])+'_current_'+ s_to_str(x[1])
def a_to_str(a):
    return str(a)

to_str_funcs={
's_to_str':x_to_str,
'a_to_str':a_to_str
}

#criticality
def h(x):
    if x[1].ball[1]==0:
        return 1
    else:
        return 0

np.random.seed(1)
n_avg=100
recent_scores=deque()
eps=.1
points_to_play=2000
q_init=0
n_simulations=20
first_index_scores_log=1
log_dir='logs/shooter_crit'
if not os.path.exists(log_dir): os.mkdir(log_dir)
for sim in range(n_simulations):
    ag = Gen_nStepAgent_crit(get_actions, q_init, to_str_funcs, h, eps=eps)
    scores_log = open(os.path.join(log_dir,"scores_"+str(first_index_scores_log+sim)+".log"), "w")
    for point in range(points_to_play):
        s_prev=None
        s = env.reset()
        x = [s_prev, s]
        ag.add_state_to_qtab(x)
        score=0
        while True:
            x=[s_prev,s]
            a = ag.choose_action_from_qtab(x,eps)
            s_, r, done = env.step(a)
            score+=r
            x_=[s,s_]
            ag.add_state_to_qtab(x_)
            ag.learn(x,a,r,x_,done)
            s_prev=s
            s=s_
            if done:
                recent_scores.append(score)
                if len(recent_scores) >n_avg:
                    recent_scores.popleft()
                if len(recent_scores) <n_avg:
                    running_mean = sum(recent_scores) / len(recent_scores)
                else: #more efficient computation of running average
                    running_mean+= (recent_scores[-1]-recent_scores[0])/n_avg
                if point%100==0:
                    print(
                        'crit, simulation nr:{}, point nr: {}, running mean: {:.2}'.format(sim, point,
                                                                                           running_mean))
                scores_log.write(str(score) + "\n")
                break
    scores_log.close()
tmp=7