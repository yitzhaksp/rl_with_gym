import json
from utils.misc import *
import matplotlib.pyplot as plt

with open('logs/scores_Tennis_nocrit_0.log', 'r') as fp:
    scrs=fp.read().splitlines()
scrs_num=[]
for x in scrs:
    scrs_num.append(int(float(x)))
scrs=scrs_num
#scrs_avgs=comp_incr_mean(np.array(scrs))
scrs_avgs=2*[None]
scrs_avgs[0]=running_mean(np.array(scrs),100)
#print(scrs_avgs)
plt.plot(scrs_avgs[0],label='ddqn')
plt.grid(True)
#plt.plot(scrs_avgs,label='kwa')

plt.legend()
plt.show()
tmp=7