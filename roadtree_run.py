from misc_roadtrees import *
from utils.RL_brain_crit import *
from utils.RL_brain_elgtrace import *
from collections import deque
import time
import numpy as np
from envs.env_tree import *
import os
import sys
method='Qlmb'
tree_shape=3

tree=get_tree(tree_shape)
env=tree_env(tree[-1])
def get_actions(s):
    return env.get_actions(s)
def s_to_str(s):
    return s.to_str()
def a_to_str(a):
    return s_to_str(a)

# criticality function
def h(s):
    if s.dist==0:
        c=1
    else:
        c=0
    return c

to_str_funcs={
's_to_str':s_to_str,
'a_to_str':a_to_str
}

np.random.seed(1)
dbg=False
n_avg=100
recent_scores=deque()
eps=.1
q_init=0
n_episodes=2000
n_simulations=20
first_index_scores_log=1
log_dir='logs/roadtree'
if not os.path.exists(log_dir): os.mkdir(log_dir)
for sim in range(n_simulations):
    if method=='Qlearn':
        ag = QAgent(get_actions, q_init, to_str_funcs, eps=eps)
    if method=='MC':
        ag = MCAgent(get_actions, q_init, to_str_funcs, eps=eps)
    elif method=='Qlmb':
        lambda_ = 0.9
        ag = QLambdaAgent(get_actions, q_init, to_str_funcs, lambda_, eps=eps)
    elif method=='crit':
        ag = Gen_nStepAgent_crit(get_actions, q_init, to_str_funcs, h, eps=eps)
    scores_log = open(os.path.join(log_dir,"scores_"+method+'_'+str(first_index_scores_log+sim)+".log"), "w")
    #episode=0
    for episode in range(n_episodes):
    #while True:
        #episode+=1
        s_root = env.reset()
        s=s_root
        ag.add_state_to_qtab(s)
        score=0
        if dbg:
            print(ag.q_tab.loc[s_to_str(s_root)])
        while True:
            a = ag.choose_action_from_qtab(s,eps)
            s_, r, done = env.step(a)
            ag.add_state_to_qtab(s_)
            score+=r
            ag.learn(s,a,r,s_,done)
            s=s_
            if done:
                recent_scores.append(score)
                if len(recent_scores) >n_avg:
                    recent_scores.popleft()
                if episode%10==0:
                    running_mean = sum(recent_scores) / len(recent_scores)
                    print('{}, simulation nr:{}, point nr: {}, running mean: {:.2}'.format(method,sim,episode, running_mean))
                scores_log.write(str(score) + "\n")
                '''
                if (episode >= 500) and (running_mean >= 1.85):
                    sys.exit("agent reached machine level")
                '''
                break
    scores_log.close()
        #time.sleep(1.0)
