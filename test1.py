import numpy as np
import sys

def comp_weighted_variance(x,p):
    weighted_mean=np.dot(x,p)
    tmp=np.square(weighted_mean*np.ones(x.size))
    return np.dot(p, np.square(x-weighted_mean*np.ones(x.size))   )

x=np.array([1,3,9])
p=np.array([.3,.2,.5])
print(comp_weighted_variance(x,p))