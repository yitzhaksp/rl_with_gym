from utils.Tennis_utils import *
from keras.models import Sequential
from keras.layers import Input, Dense, Reshape
from keras.optimizers import Adam, Adamax, RMSprop
from keras.layers.core import Activation, Dropout, Flatten
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras import losses
from keras.callbacks import ModelCheckpoint
import os
from utils.keras_utils import *
from misc_models import *
import json

#np.random.seed(1)
opt_params={'learning_rate': .001}
resume=False

append_to_batch_prob=.1 #probability that the current frameseq is appended to training batch
input_dim = [80, 80, 3]
batch_size=100
train_data_dir='data/Pong/frameseqs_and_crit/train'
validation_data_dir='data/Pong/frameseqs_and_crit/validation'

use_data_generator=False
x_val,y_val= np.load(os.path.join(validation_data_dir,'xs.npy')),np.load(os.path.join(validation_data_dir,'ys.npy'))
model_type='Tennis_crit_bigger'
n_act=3
crit_model =Crit_model(input_dim,model_type,opt_params)
with open("models/crit_model_Pong.json", "w") as outfile:
    outfile.write(crit_model.to_json())

history=History()
checkpoint = ModelCheckpoint('models/crit_model_Pong_weights_tmp.h5', monitor='val_loss', verbose=1, save_best_only=True, mode='min')

if use_data_generator:
    data_generator=simple_pong_frameseq_gen(field_length, field_width,append_to_batch_prob,batch_size)
    crit_model.fit_generator(data_generator,1,epochs=1000,validation_data=(x_val,y_val),callbacks=[checkpoint])
else:
    x_train, y_train = np.load(os.path.join(train_data_dir, 'xs_10000.npy')), np.load(os.path.join(train_data_dir, 'ys_10000.npy'))
    y_train=np.expand_dims(y_train,1)
    n_reduced=2000
    x_train,y_train=x_train[:n_reduced],y_train[:n_reduced]
    crit_model.fit(x_train,y_train,batch_size=batch_size,epochs=1000,validation_data=(x_val,y_val),callbacks=[checkpoint])

crit_model.save_weights("crit_model_1000_weights.h5", overwrite=True)

tmp=5
