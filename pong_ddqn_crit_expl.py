from utils.value_function import *
from utils.RL_utils import *
from utils.RL_deep import *
from utils.pong_utils import *
import json
import gym
import numpy as np
import os
import time
from PIL import Image as pil_img
import matplotlib.pyplot as plt
from collections import deque
from keras.models import Sequential
from keras.layers import Input, Dense, Reshape
from keras.optimizers import Adam, Adamax, RMSprop
from keras.layers.core import Activation, Dropout, Flatten
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras import losses
np.random.seed(1)
os.environ["CUDA_VISIBLE_DEVICES"]="1"



render=False
input_dim = [80,80,4]
#update_frequency = 1
learning_rate = 0.0001
#resume = False
eps_start=1.0
eps_end = .1  # exploration
eps_decay_time=10000
random_games=2000
gamma=.99
learn_freq=200
#n_ep = 200
history_batch = 1000
replay_factor = 100
start_learning = history_batch * replay_factor
max_history_len =  start_learning * 3
save_freq=100
#learn_freq=5
resume=False
dm=0 #dummy variable

class experience_object:
    def __init__(self,x,a):
        self.x=x
        self.a=a

def learning_model(input_dim, model_type=2):
    model = Sequential()
    if model_type == 0:
        #model.add(Reshape((1, 80, 80), input_shape=(input_dim,)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu'))
        model.add(Dense(n_act, activation='softmax'))
        opt = RMSprop(lr=learning_rate)
    elif model_type==1:
        #model.add(Reshape((1, 80, 80), input_shape=(input_dim,)))
        model.add(Conv2D(32, (9, 9), kernel_initializer="he_uniform", activation="relu", strides=(4, 4), padding="same"))
        model.add(Flatten())
        model.add(Dense(16, kernel_initializer="he_uniform", activation="relu"))
        model.add(Dense(n_act, activation='softmax'))
        opt = Adam(lr=learning_rate)
    elif model_type==2: #amos
        #model.add(Reshape((1, 80, 80), input_shape=(input_dim[0],input_dim[1])))
        model.add(Conv2D(32, (8, 8),input_shape=(input_dim[0],input_dim[1],input_dim[2]), strides=(4, 4), activation='relu',padding="same"))
        model.add(Conv2D(64, (4, 4), strides=(2, 2), activation='relu',padding="same"))
        model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu',padding="same"))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(n_act))
        opt = Adam(lr=learning_rate)
    model.compile(loss=losses.mean_squared_error, optimizer=opt)
    if resume == True:
        model.load_weights('model.h5')
    return model


# Define environment/game
if  not os.path.isdir('logs'):
    os.mkdir('logs')
scores_log = open("logs/scores_14oct.log", "w")
env = gym.make("Pong-v0")
n_act = env.action_space.n  # This is incorrect for Pong (?)
model_main =learning_model(input_dim)
model_trg =learning_model(input_dim)
with open("model.json", "w") as outfile:
    outfile.write(model_main.to_json())
value_func_main=ValueFunction_nn(model_main,{})
value_func_trg=ValueFunction_nn(model_trg,{})

exp_repl_par={'gamma':gamma,
              'len_memory':max_history_len}
exp_replay=experience_replay(exp_repl_par)
e=0 #epoch
curr_frame=0
tot_updates=0
win_cnt_best=0
win_cnts=[]
recent_scores=deque()
while True:
    print('len replay = {}'.format(len(exp_replay.ready_for_upd))  )
    e+=1
    eps=get_epsilon(e, random_games, eps_start, eps_end, eps_decay_time)
    s_prev, x_prev=None,None
    s=env.reset()
    s_prc=preprocess_screen(s)
    x = np.stack([s_prc,s_prc,s_prc,s_prc],2)
    ball_towardsme_prev=None
    action_values = value_func_main.comp_value(x)
    a=choose_action(action_values , eps)
    game_over,point_fin = False,False
    wait_list = []
    score=0
    i=0
    while not game_over:
        point_fin=False
        if render:
            env.render()
        if x_prev is not None:
            x = np.stack([x_prev[:,:,1],x_prev[:,:,2],x_prev[:,:,3],preprocess_screen(s)],2)
        action_values=value_func_main.comp_value(x)
        a = choose_action(action_values,eps)
        s_nxt, r, game_over, info = env.step(a)
        if r==1 or r==-1:
            point_fin=True
        score+=r
        x_nxt = np.stack([x[:,:,1],x[:,:,2],x[:,:,3],preprocess_screen(s_nxt)],2)
        action_values_nxt=value_func_main.comp_value(x_nxt)
        a_nxt_best = choose_action(action_values_nxt,0.0)
        action_values_nxt_trg=value_func_trg.comp_value(x_nxt)
        q_nxt_best=action_values_nxt_trg[a_nxt_best]
        #ball = ball_position(s)
        ball_towardsme=ball_towards(s, s_prev)
        dbg=False
        if ball_towardsme :
            exp_replay.update(x,r,a,point_fin,gamma,q_nxt_best)
        else:
            experience=experience_object(x,a)
            wait_list.append(experience)
        ball_hit_by_oponent= (ball_at_oponnent(s) and ball_towardsme and (not ball_towardsme_prev))
        if ball_hit_by_oponent or r==1:
            disc_factor=1.0
            for experience in reversed(wait_list):
                rcum=disc_factor*r
                disc_factor*=gamma
                exp_replay.update(experience.x, rcum, experience.a, point_fin,disc_factor,q_nxt_best)
            wait_list=[]

        if i%learn_freq==0 and len(exp_replay.ready_for_upd) > start_learning:
            xs,aas,rcums,done_trgs,disc_fcts,q_trgs=exp_replay.get_batch(history_batch)
            ys=[]
            for xx,aa,rcum,done_trg,disc_fct,q_trg in zip(xs,aas,rcums,done_trgs,disc_fcts,q_trgs):
                trg=comp_trg(rcum,gamma,q_trg,done_trg)
                y=value_func_main.comp_value(xx)
                # Thou shalt not correct actions not taken #deep
                y[aa]=trg
                ys.append(y)
            if ys: #list not empty?
                ys=np.vstack(ys)
                print('model update nr. {}'.format(tot_updates))
                value_func_main.learn(xs,ys)
                tot_updates+=1
            if tot_updates % save_freq == 0:
                model_main.save_weights("model.h5", overwrite=True)
                model_trg.set_weights(model_main.get_weights())
        s_prev=s
        s = s_nxt
        x_prev=x
        ball_towardsme_prev=ball_towardsme
        i+=1
    recent_scores.append(score)
    if len(recent_scores)>50:
        recent_scores.popleft()
    running_mean=sum(recent_scores)/len(recent_scores)
    print("Epoch {} | score {}".format(e, score))
    print('running mean: {}'.format(running_mean))
    #print('best wincount = {}'.format(win_cnt_best))
    scores_log.write(str(score) + "\n")
