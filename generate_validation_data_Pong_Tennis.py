from utils.Tennis_utils import *
import numpy as np
import os
from utils.Frames_generator_Pong_Tennis import *
np.random.seed(2)

data_dir='data/Pong/frameseqs_and_crit/train'
params={'env':'Pong'}
append_to_batch_prob=.1
batch_size=10000
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
data_generator=Pong_Tennis_frameseq_gen(append_to_batch_prob,batch_size,params)
xs,ys=next(data_generator)
print('saving xs ...')
np.save(os.path.join(data_dir,"xs_10000.npy"),xs)
print('saving ys ...')
np.save(os.path.join(data_dir,"ys_10000.npy"),ys)


tmp=5
