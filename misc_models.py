from keras.models import Sequential
from keras.layers import Input, Dense, Reshape
from keras.optimizers import Adam, Adamax, RMSprop
from keras.layers.core import Activation, Dropout, Flatten
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras import losses

def Q_model(input_dim,n_act ,model_type,opt_params):
    model = Sequential()
    if model_type=='Tennis_small': #small model for Tennis
        model.add(Conv2D(32, (4, 4), input_shape=(input_dim[0], input_dim[1], input_dim[2]), strides=(2, 2),
                         activation='relu', padding="same"))
        model.add(Flatten())
        model.add(Dense(256, activation='relu'))
        model.add(Dense(n_act))
    elif model_type=='Tennis_bigger': #amos, but reduced num of layers since picture is smaller
        #model.add(Reshape((1, 80, 80), input_shape=(input_dim[0],input_dim[1])))
        model.add(Conv2D(32, (4, 4),input_shape=(input_dim[0],input_dim[1],input_dim[2]), strides=(2, 2), activation='relu',padding="same"))
        model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu',padding="same"))
        model.add(Flatten())
        model.add(Dense(256, activation='relu'))
        model.add(Dense(n_act))
    elif model_type=='Tennis_big': #amos model for pong
        model.add(Conv2D(32, (8, 8),input_shape=(input_dim[0],input_dim[1],input_dim[2]), strides=(4, 4), activation='relu',padding="same"))
        model.add(Conv2D(64, (4, 4), strides=(2, 2), activation='relu',padding="same"))
        model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu',padding="same"))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(n_act))
    else:
        raise Exception('unknown model type')

    model.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))
    return model

def Crit_model(input_dim ,model_type,opt_params):
    model = Sequential()
    if model_type == 'Tennis_crit_small':  # criticality model for Tennis
        model.add(Conv2D(32, (4, 4), input_shape=(input_dim[0], input_dim[1], input_dim[2]), strides=(2, 2),
                         activation='relu', padding="same"))
        model.add(Flatten())
        model.add(Dense(256, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
    elif model_type == 'Tennis_crit_bigger':  # criticality model for Tennis
        model.add(Conv2D(32, (4, 4), input_shape=(input_dim[0], input_dim[1], input_dim[2]), strides=(2, 2),
                         activation='relu', padding="same"))
        model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu',padding="same"))
        model.add(Flatten())
        model.add(Dense(256, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
    else:
        raise Exception('unknown model type')
    model.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))
    return model

