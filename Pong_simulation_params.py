crit_func='linear'
sim_params={
'env':'Pong',
'frames_per_sample':3,
'random_seed':1,
'cuda_device':'1',
'render':False,
'resume':False,
'gamma':.99,
'opt_params':{"learning_rate": 0.0001},
'eps_start':1.0,
'eps_end' : .1,  # exploration
'eps_decay_time':10000,
'random_games':2000,
'learn_freq':200,
'history_batch':1000,
'replay_factor' : 1,
'save_freq':100,
'n_avg':100,
'e_fin':70000,
'crit_func': crit_func,
'model_type':'Tennis_small',
'model_architecture_file': 'models/Pong_qmodel_'+crit_func+'.json',
'weights_file':  'models/Pong_qmodel_'+crit_func+'_weights.h5',
}
sim_params['scores_log_path_base']='logs/scores_'+sim_params['env']+'_'+sim_params['crit_func']
sim_params['input_dim']=[80 , 80 , 3]
if sim_params['resume']:
    sim_params['n_simulations']=1
    sim_params['e_start']=5100
else:
    sim_params['n_simulations'] = 1
    sim_params['e_start'] = 0
