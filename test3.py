from keras.models import Sequential
from keras.layers import Input, Dense, Reshape
from keras.optimizers import Adam, Adamax, RMSprop
from keras.layers.core import Activation, Dropout, Flatten
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras import losses
import keras.backend as K
from utils.Tennis_utils import *

model = Sequential()
model.add(Conv2D(32, (4, 4), input_shape=(20, 40, 3), strides=(2, 2), activation='relu',
                 padding="same"))
model.add(Flatten())
model.add(Dense(1, activation='sigmoid'))
model.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=0.1))
gradients = model.optimizer.get_gradients(model.total_loss, model.trainable_weights) # gradient tensors
input_tensors = [model.inputs[0], # input data
                model.sample_weights[0], # how much to weight each sample by
                 model.targets[0], # labels
                 K.learning_phase(), # train or test mode
]

get_gradients = K.function(inputs=input_tensors, outputs=gradients)
xs,ys=np.random.rand(5,20,40,3),np.random.rand(5,1)
inputss = [
          xs, # X
         [1,1,1,1,1], # sample weights: lenght should be like x.shape[0]
          ys, # y
          0 # learning phase in TEST mode
]

gradient_array=get_gradients(inputss)
print (gradient_array)



