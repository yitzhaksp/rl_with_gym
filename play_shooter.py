import gym
from envs.env_shooter import *
from gym import Env, spaces
import time
import numpy as np
from PIL import Image as pil_image

np.random.seed(1)
width,length,gun=5,10,2
obstacle={'top':[2,2] ,'length':3 }
env=shooter_env(width,length,gun,obstacle)
points_to_play=100
for point in range(points_to_play):
    s = env.reset()
    print('point:{}'.format(point))
    while True:
        print(env.s.ball)
        a = env.action_space.sample()
        #print(a)
        s, r, done = env.step(a)
        if done:
            print('point finished, r={}'.format(r))
            break
        time.sleep(.01)

tmp=7