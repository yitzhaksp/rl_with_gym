import gym
from envs.env_Tennis import *
from gym import Env, spaces
import time
import numpy as np
from PIL import Image as pil_image

np.random.seed(1)
#env = gym.make("FrozenLake-v0")
width,length=4,8
env=simple_pong_env(length,width)
points_to_play=20
for point in range(points_to_play):
    s = env.reset()
    while True:
        img_arr=s.to_img(width,length)
        img = pil_image.fromarray(img_arr)
        img.save('pong.png')
        #img.show()
        a = env.action_space.sample()
        s, r, done = env.step(a)
        if done:
            print('point finished, r={}'.format(r))
            break
        time.sleep(60.0)

tmp=7