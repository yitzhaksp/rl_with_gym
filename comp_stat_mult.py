import json
from utils.misc import *
import matplotlib.pyplot as plt
suf_flg=1
i=0
lines=[]
file_dir= 'logs/Pong/'
file_dir='processed_score_files/Pong/'
#file_names=['ddqn_0.log','MC_0.log','lincrit_0.log','variance_crit_0.log']
file_names=['ddqn.npy','MC.npy','lincrit.npy','variance_based.npy',]
labels=['DDQN','Monte Carlo','CVS (human)','CVS (environment)']
file_type=file_names[0].split('.')[1]
running_mean_flg=True #perform averaging via running mean
running_mean_window=100
for i,file_name in enumerate(file_names):
    if file_type=='npy':
        scores_num=np.load(file_dir + file_name)
    elif file_type=='log':
        with open(file_dir+file_name, 'r') as fp:
            scores = fp.read().splitlines()
        scores_num = [int(float(x)) for x in scores]
    else:
        raise Exception('unknown filetype: '+ file_type)
    #scores_num=scores_num[:1000]
    if running_mean_flg:
        scores_for_plt = running_mean(np.array(scores_num), running_mean_window)
    else:
        scores_for_plt=scores_num
    lines+=plt.plot(scores_for_plt,label=labels[i])
#labels = [l.get_label() for l in lines]
#plt.legend(lines,labels)
plt.grid(True)
plt.xlabel('episode')
plt.ylabel('score')
plt.legend()
#plt.savefig('plots/test.png')
plt.show()
tmp=7