import numpy as np
from utils.TileCoding import *



# get best action
def getAction(s,valueFunction,par):
    if np.random.binomial(1, par['EPSILON']) == 1:
        return np.random.choice(par['ACTIONS'])
    action_values = []
    for action in par['ACTIONS']:
        action_values.append(valueFunction.comp_value(s, action))
    best_actions= [action_ for action_, value_ in zip(par['ACTIONS'],action_values) if value_ == np.max(action_values)]
    return np.random.choice(best_actions)


def NStepSarsa(s_0,valueFunction,env,par, n=1):
    # get initial action
    a = getAction(s_0, valueFunction,par)

    # track previous position, velocity, action and reward
    states = [s_0]
    actions = [a]
    rewards = [0.0]
    time = 0

    # the length of this episode
    T = float('inf')
    while True:
        time += 1
        if time < T:
            # take current action and go to the new state
            s_, reward, done, info = env.step(a)
            states.append(s_)
            # choose new action
            a_ = getAction(s_, valueFunction,par)
            actions.append(a_)
            rewards.append(reward)

            if done:
                T = time

        # get the time of the state to update
        updateTime = time - n
        if updateTime >= 0:
            update_target = 0.0
            # calculate corresponding rewards
            for t in range(updateTime + 1, min(T, updateTime + n) + 1):
                update_target += rewards[t]
            # add estimated state action value to the return
            if updateTime + n <= T:
                update_target += valueFunction.value(states[updateTime + n],actions[updateTime + n])
            # update the state value function
            if states[updateTime] != 'terminal':
                valueFunction.learn(states[updateTime], actions[updateTime], update_target)
        if updateTime == T - 1:
            break
        s,a=s_, a_

    return time


def SarsaOrQLambda(s0,valueFunction,term_val,env,par,SoQ,dbg=False):
    assert (SoQ=='s' or SoQ=='q')
    valueFunction.elg_tr*=0
    s,a = s0, getAction(s0, valueFunction,par)
    rcum = 0
    if dbg:
        print('w: ',valueFunction.w)
    while True:
        if par['render']:
            env.render()
        s_, r, done = env.step(a)
        a_ = getAction(s_, valueFunction,par)
        if not done:
            if SoQ=='s':
                update_target=r+par['gamma']*valueFunction.comp_value(s_,a_)
            elif SoQ=='q':
                act_values=[valueFunction.comp_value(s_,aa) for aa in par['ACTIONS']]
                update_target = r + par['gamma'] * max(act_values)
        else:
            update_target=r+term_val


        #if s != 'terminal':
        valueFunction.learn_elg(s, a, update_target)
        rcum+=r
        if done:
            break
        s,a=s_, a_
    return rcum

