import keras
import keras.backend as K
from keras.models import load_model, model_from_json


def load_model_composite(model_file,weights_file):
    json_file = open(model_file, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights(weights_file)
    return model

def get_gradient_tens(model):
    """Return the gradient of every trainable weight in model

    Parameters
    -----------
    model : a keras model instance

    First, find all tensors which are trainable in the model. Surprisingly,
    `model.trainable_weights` will return tensors for which
    trainable=False has been set on their layer (last time I checked), hence the extra check.
    Next, get the gradients of the loss with respect to the weights.

    """
    #weights = [tensor for tensor in model.trainable_weights if model.get_layer(tensor.name[:-2]).trainable]  (original)
    weights=model.trainable_weights # yitzhak version
    return model.optimizer.get_gradients(model.total_loss, weights)


def get_gradient_func(model):
    weights = model.trainable_weights  # yitzhak version
    optimizer = model.optimizer
    gradient_tensor=optimizer.get_gradients(model.total_loss, weights)
    input_tensor=[
        model.total_loss,
        model.trainable_weights
    ]

    return K.function(inputs=input_tensor,
                      outputs=model.optimizer.get_gradients(model.total_loss, model.trainable_weights))



class History(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.gradients = []

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        inputss = [
            self.model.total_loss,
            self.model.trainable_weights
            ]
        current_gradient_func=get_gradient_func(self.model)
        self.gradients.append(current_gradient_func(inputss))
        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return