from .RL_utils import *
from .RL_deep import *
from envs.env_Tennis import *
from utils.Ballgame_utils import *

class Tennis_funcs(Ballgame_funcs):

    def state_to_img(self,s,params):
        return s.to_img(params['field_width'],params['field_length'])

    def ball_position(self, s):
        return s.ball

    def ball_towards(self,s,s_prev):
        if s.velocity[1]>0:
            return True
        else:
            return False

    def ball_in_field(self,ball, env_params):
        in_field = ball[1] > 0 and ball[1] < (env_params['field_length'] - 2)
        return in_field

#####################################
# functions that don't belong to the class
#####################################


