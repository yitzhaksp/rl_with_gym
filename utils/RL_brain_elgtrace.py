"""
This part of code is the Q learning brain, which is a brain of the agent.
All decisions are made in here.

View more on my tutorial page: https://morvanzhou.github.io/tutorials/
"""

import numpy as np
import pandas as pd
from .misc import *
from .RL_brain_general import *


class ElgTraceAgent(GenLearnAgent):
    def __init__(self, get_actions,q_init,to_str_funcs,lambda_,
                 alpha=0.1, gamma=1.0, eps=0.1,eps_decay=1.0,dbg=False):
        GenLearnAgent.__init__(self,get_actions, q_init, to_str_funcs,
                                                  alpha=alpha, gamma=gamma, eps=eps,eps_decay=eps_decay, dbg=dbg)
        # super(ElgTraceAgent,self).__init__(get_actions, q_init, terminal, to_str_funcs,
        #                                           alpha=alpha, gamma=gamma, eps=eps,eps_decay=eps_decay, dbg=dbg)
        self.lambda_=lambda_
        self.elgtr=pd.DataFrame()

    def add_state_to_elgtr(self,s):
        actions_str=[self.a_to(a) for a in self.get_actions(s)]
        if self.s_to(s) not in self.elgtr.index:
            self.elgtr = self.elgtr.append(pd.Series(
                    [0] * len(actions_str),
                    index=actions_str,name=self.s_to(s)) )

class SarsaLambdaAgent(ElgTraceAgent):

    def learn(self, s, a, r, s_, a_):
        assert(self.s_to(s) in self.q_tab.index)
        assert(self.s_to(s_) in self.q_tab.index)
        self.add_state_to_elgtr(s)
        self.add_state_to_elgtr(s_)
        q_predict = self.q_tab.loc[self.s_to(s), self.a_to(a)]
        if not(self.terminal(s_)):
            q_target = r + self.gamma * self.q_tab.loc[self.s_to (s_), self.a_to( a_)]  # next state is not terminal
        else:
            q_target = r  # next state is terminal
        error = q_target - q_predict
        self.elgtr.loc[self.s_to(s), :] *= 0
        self.elgtr.loc[self.s_to(s), self.a_to(a)] = 1
        self.q_tab += self.alpha * error * self.elgtr
        self.elgtr *= self.gamma * self.lambda_
        if self.terminal(s_):
            self.elgtr *=0



class QLambdaAgent(ElgTraceAgent):
    def learn(self, s, a, r, s_,done):
        assert(self.s_to(s) in self.q_tab.index)
        assert(self.s_to(s_) in self.q_tab.index)
        self.add_state_to_elgtr(s)
        self.add_state_to_elgtr(s_)
        q_predict = self.q_tab.loc[self.s_to(s), self.a_to(a)]
        if not done:
            best_a_ = self.choose_action_from_qtab(s_, 0.0)
            q_target = r + self.gamma * self.q_tab.loc[self.s_to(s_), self.a_to(best_a_)]  # next state is not terminal
        else:
            q_target = r  # next state is terminal
        error = q_target - q_predict
        self.elgtr.loc[self.s_to(s), :] *= 0
        self.elgtr.loc[self.s_to(s), self.a_to(a)] = 1
        self.q_tab += self.alpha * error * self.elgtr
        self.elgtr *= self.gamma * self.lambda_
        if done:
            self.elgtr *= 0

