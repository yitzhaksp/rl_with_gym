import numpy as np

class ValueFunctionTC:
    # In this example I use the tiling software instead of implementing standard tiling by myself
    # One important thing is that tiling is only a map from (state, action) to a series of indices
    # It doesn't matter whether the indices have meaning, only if this map satisfy some property
    # View the following webpage for more information
    # http://incompleteideas.net/sutton/tiles/tiles3.html
    # @maxSize: the maximum # of indices
    def __init__(self, stepSize, bounds, par, numOfTilings=8, maxSize=2048):
        self.maxSize = maxSize
        self.numOfTilings = numOfTilings

        # divide step size equally to each tiling
        self.stepSize = stepSize / numOfTilings
        self.gamma=par['gamma']
        self.lamda=par['lamda']
        self.hashTable = IHT(maxSize)

        # weight for each tile
        self.weights = np.zeros(maxSize)
        self.elg_tr = np.zeros(maxSize)

        # min and max for each feature
        self.bounds=bounds

        self.featScale=self.numOfTilings / (bounds[1] - bounds[0])
    # get indices of active tiles for given state and action
    def getActiveTiles(self,s, action):
        s= self.featScale*(s-self.bounds[0])
        activeTiles = tiles(self.hashTable, self.numOfTilings,s,[action])
        return activeTiles

    # estimate the value of given state and action
    def value(self, s, action):
        if s == 'terminal':
            return 0.0
        activeTiles = self.getActiveTiles(s, action)
        return np.sum(self.weights[activeTiles])

    def learn(self, s, action, target):
        activeTiles = self.getActiveTiles(s, action)
        estimation = np.sum(self.weights[activeTiles])
        delta = self.stepSize * (target - estimation)
        for activeTile in activeTiles:
            self.weights[activeTile] += delta

    def learn_elg(self, s, a, target):
        activeTiles = self.getActiveTiles(s, a)
        self.elg_tr[activeTiles]=1
        estimation = np.sum(self.weights[activeTiles])
        delta = self.stepSize * (target - estimation)
        self.weights += delta*self.elg_tr
        self.elg_tr=self.gamma * self.lamda* self.elg_tr
    # get # of steps to reach the goal under current state value function
    def costToGo(self, s):
        costs = []
        for action in ACTIONS:
            costs.append(self.value(s, action))
        return -np.max(costs)

