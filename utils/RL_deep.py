import numpy as np
from .misc import *

class wait_list_object:
    def __init__(self,x,r,a,done_trg,disc_fct):
        self.x = x  # x=to_feat(s)
        self.rcum = r
        self.a = a
        self.done_trg=done_trg
        self.disc_fct=disc_fct # discount factor for the next reward
        self.q_trg=None




class wait_list_object_crit(wait_list_object):
    def __init__(self,x,r,a,done_trg,disc_fct):
        super().__init__( x,r,a,done_trg,disc_fct)
        self.crit_cum=0.0
        # if crit>=1.0:
        #     self.q_trg=q_nxt
        # else:
        #     self.q_trg=None


class experience_replay():
    def __init__(self,par):
        self.wait_list=[] # for attention alogorithm
        self.ready_for_upd=[]
        self.gamma=par['gamma']
        self.len_memory=par['len_memory']
        self.delete_buffer=100
    def add_to_list(self,some_list,new_rec):
        some_list.append(new_rec)
        if len(some_list) > self.len_memory:
            some_list=some_list[self.delete_buffer:]
        return some_list
    def update(self,x,rcum,a,done_trg,q_trg):
        new_rec=wait_list_object(x,rcum,a,done_trg,self.gamma)
        new_rec.q_trg=q_trg
        self.ready_for_upd=self.add_to_list(self.ready_for_upd,new_rec)


    def update_crit(self,x,r,a,done_nxt,q_nxt,crit):
        assert(crit>=0)
        for this_obj in self.wait_list:
            this_obj.rcum+=this_obj.disc_fct*r
            this_obj.disc_fct*=self.gamma
            this_obj.crit_cum+=crit
        dummy=False
        new_rec=wait_list_object_crit(x,r,a,dummy,self.gamma)
        self.wait_list=self.add_to_list(self.wait_list,new_rec)
        ind_del=[]
        for idx,this_obj in enumerate(self.wait_list):
            if this_obj.crit_cum>=1.0 or done_nxt:
                this_obj.q_trg=q_nxt
                this_obj.done_trg=done_nxt
                self.ready_for_upd=self.add_to_list(self.ready_for_upd, this_obj)
                ind_del.append(idx)
        remove_from_list(self.wait_list,ind_del)

    def update_mc(self, x, r, a, done_nxt ):
        for this_obj in self.wait_list:
            this_obj.rcum += this_obj.disc_fct * r
            this_obj.disc_fct *= self.gamma
        dummy=False
        new_rec=wait_list_object(x,r,a,dummy,self.gamma)
        self.wait_list=self.add_to_list(self.wait_list,new_rec)
        if done_nxt:
            for this_obj in self.wait_list:
                    this_obj.q_trg = None
                    this_obj.done_trg = True
                    self.ready_for_upd = self.add_to_list(self.ready_for_upd, this_obj)
            self.wait_list=[]



    def get_batch_inner(self,somelist,ind):
        xs,aas,rcums,done_trgs,disc_fcts,q_trgs=[],[],[],[],[],[]
        #if ind.size()>0:
        for i in ind:
            xs.append(somelist[i].x)
            aas.append(somelist[i].a)
            rcums.append(somelist[i].rcum)
            done_trgs.append(somelist[i].done_trg)
            q_trgs.append(somelist[i].q_trg)
            disc_fcts.append(somelist[i].disc_fct)
        return np.array(xs), aas, rcums, done_trgs,disc_fcts,np.array(q_trgs)

    def get_batch(self,batch_size):
        if self.ready_for_upd:
            ind =np.random.randint(0, len(self.ready_for_upd),size=batch_size)
        else:
            ind=np.array([])
        return self.get_batch_inner(self.ready_for_upd,ind)

