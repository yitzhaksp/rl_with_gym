import numpy as np
import numpy.linalg as la
import math

# get number of lines
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def remove_from_list(x,ind):
    ind.sort()
    for idx in reversed(ind):
        del x[idx]



def setdiff(A,B):
    C=[]
    for a in A:
        if a not in B:
            C.append(a)
    return C

def isnan(x):
    try:
        if math.isnan(x):
            return True
        else:
            return False
    except:
        return True


# delete all nan entries from pandas series
def series_get_numeric(ser):
    for ind in ser.index:
        if isnan(ser[ind]):
            ser = ser.drop(ind)
    return ser



#distance in maze
def state_dist(s1,s2):
    return abs(s1[0]-s2[0]) + abs(s1[1]-s2[1])

#joins 2 dictionaries with identical keys
def join_dict_of_lst(x,y):
    for key in x.keys():
        assert (key in y)
    for key in y.keys():
        assert (key in x)
    z={}
    for key in x.keys():
        z[key]=x[key]+y[key] # merge lists
    return z

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)

#compute mean until n-th number in array
def comp_incr_mean(x):
    cumsum=np.cumsum(x)
    rng=np.arange(x.size)+1
    return cumsum/rng
# tr is the matrix which contains all states
# from human_demo for a given action, where each row is a state
def comp_min_dist(s,a,tr):
    if not tr[a]:
        return float('inf')
    else:
        tr_a=np.array(tr[a])
        return np.min(la.norm(tr_a-s,axis=1))

def comp_shap_rew(s,a,tr):
    min_dist=comp_min_dist(s,a,tr)
    return math.exp(-min_dist**2)

def comp_weighted_variance(x,p):
    weighted_mean=np.dot(x,p)
    return np.dot(p, np.square(x-weighted_mean*np.ones(x.size))   )
