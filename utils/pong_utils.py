from .RL_utils import *
from .RL_deep import *
from utils.Ballgame_utils import *

class Pong_funcs(Ballgame_funcs):

    def state_to_img(self,s,params):
        return preprocess_screen(s)


    def ball_position(self,s):
        if s is not None:
            img=crop_screen(s)
            return ball_position_inner(img)
        else:
            return None


    def ball_towards(self,s,s_prev):
        ball = self.ball_position(s)
        ball_prev = self.ball_position(s_prev)
        if (ball != None) and (ball_prev != None):
            towards = ball[1] > ball_prev[1]
        else:
            towards = False
        return towards

    def ball_in_field(self,ball,params):
        in_field = (ball[1] > 9) and (ball[1] < 70)
        return in_field

###################################
# functions that don't belong to the class
#####################################
def dist_to_agents_baseline(ball):
    return 70-ball[1]


def ball_at_oponnent(s):
    ball=ball_position(s)
    if (ball is not None) and ball[1]<17:
        return True
    else:
        return False


def ball_position_whole(img):
    img[159,:,1]=0
    rows,cols=np.where(img[:,:,1]==236)
    return rows,cols

def ball_position_inner(img):
    rows,cols=np.where(img[:,:,1]==236)
    if rows.size>0:
        return rows.max(),cols.max()
    else:
        return None

def touch_my_paddle(s):
    img = crop_screen_origsize(s)
    rows,cols = ball_position_whole(img)
    if rows.size>0:
        maxcol=max(cols)
        if maxcol>=137 and maxcol<=139:
            img1=img[:,:,1]
            if any(img1[row , 140]==186 for row in rows):
                return True
    return False

def crop_screen_origsize(I):
    I = I[35:195]
    return I

def crop_screen(I):
    I = crop_screen_origsize(I)
    I = I[::2, ::2, :]
    return I

def preprocess_screen(I,flt=False):
    I=crop_screen(I)
    I=I[:,:,0]
    I[I == 144] = 0
    I[I == 109] = 0
    I[I != 0] = 1
    I = I.astype(np.float)
    if not flt:
        return I
    return I.ravel()

def comp_feat(s,s_prev):
    x_prev=preprocess_screen(s_prev)
    x=preprocess_screen(s)
    return np.expand_dims(x-x_prev,0)
'''
def comp_crit_bin(s,s_prev):
    if s_prev is not None:
        atn=int(ball_towards(s,s_prev))
    else:
        atn=0.0

    return atn


def comp_crit_linear(s,s_prev):
    ball=ball_position(s)
    if (ball is not None) and ball_towards(s,s_prev) :
        #maximal possible dist from ball to myracketlevel when ball is in the field (not beyond the field limits)
        max_dist_ball_mybaseline=60
        dist_ball_mybaseline = 70 - ball[1]
        if ball[1]>9 and ball[1]<70: #ball in field and didn't yet touch my racket
            # we want crit=1.0 when dist_ball_myracketlevel==1 and crit=0 when ball at opponent
            crit=1-float(dist_ball_mybaseline-1)/(max_dist_ball_mybaseline-1)
        else:
            crit=0.0
    else:
        crit=0.0
    return crit

'''

