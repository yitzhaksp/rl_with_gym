import numpy as np
import numpy.linalg as la

class ValueFunction_lin:
    def __init__(self, w0,alpha, par):

        # divide step size equally to each tiling
        self.alpha = alpha
        self.gamma=par['gamma']
        self.lamda=par['lamda']
        # weight for each tile
        self.wdim=w0.size
        self.w = w0
        self.elg_tr = np.zeros(self.wdim)

    def to_feat(self,s,a):
        tmp1=np.concatenate((s.to_feat(),a.to_feat(),np.array([1.0])))
        return tmp1

    # linear func approx
    def comp_value(self, s, a):
        tmp=self.to_feat(s,a)
        return np.dot(self.w,self.to_feat(s,a))

    # linear func approx
    def comp_grd(self,s,a):
        return self.to_feat(s,a)

    def learn(self, s,a, target):
        q = self.comp_value(s,a)
        self.w+=self.alpha*(target-q)*self.comp_grd(s,a)

    def learn_elg(self, s, a, target):
        dbg=1
        self.elg_tr+=self.comp_grd(s,a)
        q = self.comp_value(s,a)
        delta = self.alpha * (target - q)
        if dbg:
            print("elgtr_norm: ",la.norm(self.elg_tr))
        self.w += delta * self.elg_tr
        self.elg_tr*=self.gamma * self.lamda

class ValueFunction_batch:
    def __init__(self, par):
        pass

    def comp_values(self,xs):
        pass

    def learn(self,xs,ys):
        pass

#keras model
class ValueFunction_nn(ValueFunction_batch):
    def __init__(self,model,par):
        super(ValueFunction_nn, self).__init__(par)
        self.model=model

    def comp_value(self,x):
        x=np.expand_dims(x,0)
        return np.squeeze(self.model.predict(x))

    def comp_values(self,xs):
        return self.model.predict(xs)


    def learn(self, xs, ys):
        self.model.train_on_batch(xs, ys)



