import gym
from utils.Tennis_utils import *
from envs.env_Tennis import *
from utils.pong_utils import *
from utils.critfuncs import *



def Pong_Tennis_frameseq_gen(append_to_batch_prob,batch_size,params):
    if params['env'] == 'Tennis':
        env = simple_pong_env(params['field_length'], params['field_width'])
        thisgame_funcs = Tennis_funcs()
    elif params['env'] == 'Pong':
        env = gym.make('Pong-v0')
        thisgame_funcs = Pong_funcs()
    s_prev = None
    s = env.reset()
    s_prc=thisgame_funcs.state_to_img(s, params)
    x = np.stack([s_prc, s_prc, s_prc], 2)
    xs, ys = [], []
    samp_num=0
    while True:
        if np.random.rand()<=append_to_batch_prob:
            samp_num+=1
            print('sample number {}'.format(samp_num))
            xs.append(x)
            ys.append(comp_crit_bin(s, s_prev,thisgame_funcs, params))
        if len(xs)==batch_size:
            samp_num=0
            print('yielding ...')
            yield np.stack(xs,0),np.stack(ys,0)
            xs,ys=[],[]
        a = env.action_space.sample()
        s_nxt, r, done,info = env.step(a)
        x = np.stack([x[:, :, 1], x[:, :, 2], thisgame_funcs.state_to_img(s_nxt, params)], 2)
        #s_prev='dummy' # make sure that s_prev is not None
        s_prev=s
        s=s_nxt
        if done:
            s = env.reset()
            s_prc = thisgame_funcs.state_to_img(s, params)
            x = np.stack([s_prc, s_prc, s_prc], 2)
            s_prev=None
