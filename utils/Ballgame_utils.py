from .RL_utils import *
from .RL_deep import *
from envs.env_Tennis import *
class Ballgame_funcs():

    def state_to_img(self,s,params):
        pass


    def ball_towards(self,s):
        pass


    def comp_crit_bin(self,s,s_prev):
        pass

    def comp_crit_linear(self,s,s_prev,field_length):
        pass

    def comp_crit_rand(self,s,s_prev,crit_params):
        pass


def replay_episode(env,Q,actions,thisgame_funcs,sim_params,eps=0):
    s_prev, x_prev = None, None
    s = env.reset()
    s_prc = thisgame_funcs.state_to_img(s, sim_params)
    x = np.stack(sim_params['frames_per_sample'] * [s_prc], 2)
    done = False
    score = 0
    while not done:
        if x_prev is not None:
            aux = []
            for i in range(1, x.shape[2]):
                aux.append(x_prev[:, :, i])
            aux.append(thisgame_funcs.state_to_img(s, sim_params))
            x = np.stack(aux, 2)
        action_values = Q.comp_value(x)
        a = choose_action(actions, action_values, eps)
        s_nxt, r, done, info = env.step(a)
        score += r
        s = s_nxt
        x_prev = x
    return score