import numpy as np

def comp_crit_bin(s,s_prev,ballgame_funcs_obj,crit_params):
    if s_prev is not None:
        atn=int(ballgame_funcs_obj.ball_towards(s,s_prev))
    else:
        atn=0.0
    return atn

def comp_crit_rand(s,s_prev,ballgame_funcs_obj,crit_params):
    if crit_params['randcrit_type']=='mixed':
        crit= np.random.randint(2)
    elif crit_params['randcrit_type'] == 'barrier':
        pass
    else:
        raise Exception('unknown randcrit_type '+crit_params['randcrit_type'] )
    return crit

def comp_crit_linear(s,s_prev,ballgame_funcs_obj,crit_params):
    if  ballgame_funcs_obj.ball_towards(s, s_prev) :
        #maximal possible dist from ball to myracketlevel when ball is in the field (not beyond the field limits)
        ball = ballgame_funcs_obj.ball_position(s)
        if crit_params['env']=='Pong':
            ball = ballgame_funcs_obj.ball_position(s)
            max_dist_ball_mybaseline=60
            dist_ball_mybaseline = 70 - ball[1]
        elif crit_params['env'] == 'Tennis':
            ball=s.ball
            max_dist_ball_mybaseline=crit_params['field_length']-3
            dist_ball_mybaseline=(crit_params['field_length'] - 2) - ball[1]
        #compute criticality
        if ballgame_funcs_obj.ball_in_field(ball,crit_params): #ball in field and didn't yet touch my racket
            # we want crit=1.0 when dist_ball_myracketlevel==1 and crit=0 when ball at opponent
            crit=1-float(dist_ball_mybaseline-1)/(max_dist_ball_mybaseline-1)
        else:
            crit=0.0
    else:
        crit=0.0
    return crit


def comp_crit_variance_based(variance,max_encountered_variance):
    if max_encountered_variance==0:
        crit=0.0
    else:
        crit=variance/max_encountered_variance
    return  crit

