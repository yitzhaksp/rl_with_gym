# RL_brain for attention learning

from .RL_brain_general import *

class Gen_nStepAgent_crit(GenLearnAgent):
    def __init__(self, get_actions,q_init,to_str_funcs,h,
                 alpha=0.1, gamma=1.0, eps=0.1,dbg=False):
        super(Gen_nStepAgent_crit, self).__init__(get_actions,q_init,to_str_funcs,
                                               alpha=alpha, gamma=gamma, eps=eps,dbg=dbg)
        self.h=h
        self.wait_list=pd.DataFrame()
    def learn(self, s, a, r, s_,done):
        assert(self.s_to(s) in self.q_tab.index)
        assert(self.s_to(s_) in self.q_tab.index)
        if not ( self.sa_to(s, a) in self.wait_list.index ):
            self.wait_list=self.wait_list.append(pd.Series([0,0,0], index=['crit','T','R'], name=self.sa_to(s, a)))
        if (not done) :
            for sa_hat in self.wait_list.index:
                self.wait_list.loc[sa_hat,'R']+=self.gamma**self.wait_list.loc[sa_hat,'T']*r
                self.wait_list.loc[sa_hat,'T']+=1
                if self.wait_list.loc[sa_hat,'crit'] >= 1.0:
                    q_s_ = self.q_tab.loc[self.s_to(s_)].values
                    y=self.wait_list.loc[sa_hat,'R']+self.gamma**self.wait_list.loc[sa_hat,'T']*max(q_s_[~np.isnan(q_s_)])
                    s_hat_str,a_hat_str=self.sa_str_sep(sa_hat)
                    error=y-self.q_tab.loc[s_hat_str, a_hat_str]
                    if self.dbg:
                        print('s_hat=', s_hat_str)
                        print('update with error=', error)
                    self.q_tab.loc[s_hat_str, a_hat_str]+= self.alpha * error
                    self.wait_list=self.wait_list.drop(sa_hat)
            for sa_hat in self.wait_list.index:
                self.wait_list.loc[sa_hat,'crit'] += self.h(s_)

        else:
            for sa_hat in self.wait_list.index:
                self.wait_list.loc[sa_hat,'R']+=self.gamma**self.wait_list.loc[sa_hat,'T']*r
                y = self.wait_list.loc[sa_hat,'R'] + self.gamma ** self.wait_list.loc[sa_hat,'T'] * 0
                s_hat_str, a_hat_str = self.sa_str_sep(sa_hat)
                error = y - self.q_tab.loc[s_hat_str, a_hat_str]
                if self.dbg:
                    print('s_hat=',s_hat_str)
                    print('update with error=',error)
                self.q_tab.loc[s_hat_str, a_hat_str]+= self.alpha * error
            self.wait_list = pd.DataFrame()
