'''
SARSA with attention on random walk
'''
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain_approx import *
from utils.value_function import *
from envs.env_road import *

env=road_env(20,[10])
k0=1
pos0=0
q_init=0
alpha=0.1
dbg=0
algs=[1]
SoQ='q'
n_sims=1
n_epochs=200
scores = np.zeros((len(algs),n_sims, n_epochs))
sim_info={
'alg_names':[None for i in algs]
}

w0=np.zeros(4)
for i_ep in range(n_epochs):
    env.set_state(pos0)
    rcum=0
    while True:
        a=np.random.choice(env.get_actions(env.s))
        s_, r, done=env.step(a)
        rcum+=r
        if done:
            break
    print('episode:{}, score:{}'.format(i_ep, rcum))

with open('sim_info.json', 'w') as fp:
    json.dump(sim_info, fp)
#plt.plot(scores)
#plt.show()
print('scores={}'.format(scores))
