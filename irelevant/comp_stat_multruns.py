import json
from utils.misc import *
import matplotlib.pyplot as plt
suf_flg=1
i=0
lines=[]
sufsuf=['','','','']
#suf=['mc','at']
#suf=['best_a_lmb']
with open('sim_info.json', 'r') as fp:
    sim_info = json.load(fp)
scrs=np.load('scores.npy')
for i_alg,suf in enumerate(sim_info['alg_names']):
    scrs_avg=np.mean(scrs[i_alg],axis=0)
    lines+=plt.plot(scrs_avg,label=suf+sufsuf[i_alg])
#labels = [l.get_label() for l in lines]
#plt.legend(lines,labels)
plt.legend()
plt.savefig('plots/test.png')
plt.show()
tmp=7