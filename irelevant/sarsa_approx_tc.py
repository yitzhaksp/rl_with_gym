# SARSA with tile coding

from utils.RL_brain_approx import *
import matplotlib.pyplot as plt
import gym
import json

episodes = 5
numOfTilings = 8
alpha = 0.1
alg='s'
env_name='MountainCar-v0'
env = gym.make(env_name)

if env_name=='CartPole-v0':
    bounds=[[-2.4, -2,-1,-3.5], [2.4,2,1,3.5]]
elif env_name=='MountainCar-v0':
    bounds=[[-1.2,-.07], [.6,.07]]
bounds=np.array(bounds)
n_steps_list = []
s0= ((bounds[0]+bounds[1])/2).tolist()
par={'ACTIONS':list(range(env.action_space.n)),'EPSILON':.1,
     'gamma':1.0,'lamda':.8, 'render':1}
valueFunc = ValueFunctionTC(alpha,bounds,par,numOfTilings)
for episode in range(episodes):
    env.reset()
    n_steps=SarsaOrQLambda(s0,valueFunc,env,par,alg)
    n_steps_list.append(n_steps)
    print( 'episode:', episode,'; n_steps:',n_steps)


with open('scores.json', 'w') as fp:
    json.dump(n_steps_list, fp)
info={'eps':par['EPSILON'],
     'gamma':par['gamma'],
     'alpha':alpha,
     'lamda':par['lamda'],
      'n_tilings':numOfTilings,
     'alg':alg}
with open('scores_info.json', 'w') as fp:
    json.dump(info, fp)

plt.figure(1)
plt.plot(n_steps_list, label='alpha = '+str(alpha)+'/'+str(numOfTilings))
plt.xlabel('Episode')
plt.ylabel('Steps per episode')
plt.legend()
plt.show()