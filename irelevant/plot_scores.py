import matplotlib.pyplot as plt
import json

with open('scores_at.json', 'r') as fp:
    scrs = json.load(fp)
plt.plot(scrs)
plt.show()