import numpy as np
from envs.random_walk import *

length=15
first_double_state=7
n_double_states=3
env=RWEnv(length,
         first_double_state,
         n_double_states)
s=(5,'sng')
env.set_state(s)
for i in range(100):
    print(i)
    print('state=',s)
    tmp=env.RW_graph[s]
    a_ind=np.random.randint(0,len(env.RW_graph[s]))
    a=env.RW_graph[s][a_ind]
    s_, r, done = env.step(a)
    s=s_
    if done:
        break
