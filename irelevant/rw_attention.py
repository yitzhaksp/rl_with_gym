'''
SARSA with attention on random walk
'''
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain_general import *
from envs.random_walk import *
np.random.seed(42)

length=100
first_double_state=55
n_double_states=30
env=RWEnv(length,
         first_double_state,
         n_double_states)
s0=(52,'sng')
scores = []
q_init=-200
lambda_=.95
h=set_h_rw(env.RW_graph,length)
s_term=[(0,'g'),(length-1,'g')]
to_str_funcs={
's_to_str':s_to_str_rw,
'a_to_str':a_to_str_rw
}
dbg=0
alg=3
if alg==1:
    ag = GenSarsaAgent(env.get_actions, q_init, s_term, to_str_funcs, dbg=dbg)
elif alg==2:
    ag = GenSarsaLambdaAgent(env.get_actions, q_init, s_term, to_str_funcs, lambda_, dbg=dbg)
elif alg==3:
    ag = Gen_nStepAgent_crit(env.get_actions,q_init,s_term,to_str_funcs,h,dbg=dbg)

for i_ep in range(100):
    env.set_state(s0)
    s=s0
    a = ag.choose_action(s)
    t=0
    while True:
        #print('t={}'.format(t))
        s_, r, done = env.step(a)
        a_ = ag.choose_action(s_)
        ag.learn(s,a,r,s_,a_)
        s,a=s_,a_
        t+=1
        if done:
            print('episode:{}, score:{}'.format(i_ep,t))
            scores.append(int(t))
            break

with open('scores_lmb1.json', 'w') as fp:
    json.dump(scores, fp)
plt.plot(scores)
plt.show()
print('scores={}'.format(scores))
