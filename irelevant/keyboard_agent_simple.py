import gym
import time
env = gym.make('FrozenLake-v0')

action_list=env.action_space
n_act = env.action_space.n
print('n_act={}'.format(n_act))
env.reset()
t=0
while 1:
    env.render()
    a = int(input("next action ? "))
    s, r, done, info = env.step(a)
    print('r=', r, '\n s=',s)
    t+=1
    #time.sleep(.5)
    if done:
        print("Episode finished after {} timesteps".format(t))
        break