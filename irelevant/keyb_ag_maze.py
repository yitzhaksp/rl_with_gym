from gym_maze.envs import MazeEnv
from gym_maze.envs.generators import *
import time
import matplotlib.pyplot as plt
import json

action_button={'w':0,'s':1,'a':2,'d':3}
maze = RandomBlockMazeGenerator(maze_size=8, obstacle_ratio=0.0)
init_and_goal_states={'i':[3,8],'g':[[8,4]]}
env = MazeEnv(maze,init_and_goal_states=init_and_goal_states)
hd={}
for a in env.all_actions:
    hd[a]=[]
s_view=env.reset()
n_steps=0
while 1:
    plt.imshow(s_view, cmap=env.cmap, norm=env.norm)
    plt.show()
    valid_inp=False
    while not valid_inp:
        inp=input("next action ? ")
        if inp in action_button:
            a = action_button[inp]
            hd[a].append(env.state)
            valid_inp=True
        else:
            print('invalid input')
    s_view, r, done, info = env.step(a)
    print('state=',env.state)
    n_steps+=1
    if done:
        break
with open('human_demo_2.json', 'w') as fp:
    json.dump(hd, fp)