'''
Q-learning approach for different RL problems
as part of the basic series on reinforcement learning @
https://github.com/vmayoral/basic_reinforcement_learning
Inspired by https://gym.openai.com/evaluations/eval_kWknKOkPQ7izrixdhriurA
        @author: Victor Mayoral Vilches <victor@erlerobotics.com>
'''
import gym
import numpy
import random
import pandas
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain import *
env_name='MountainCar-v0'
env = gym.make(env_name)
n_feat = env.observation_space.shape[0]
lambda_learning=0


def build_state(features):
    return int("".join(map(lambda feature: str(int(feature)), features)))


def to_bin(value, bins):
    return numpy.digitize(x=[value], bins=bins)[0]

n_bins=10 # num of bins for each state dimension
#mins and maxs of each state-dimension
state_bins=[]
if env_name=='CartPole-v0':
    bound=[[-2.4, -2,-1,-3.5], [2.4,2,1,3.5]]
elif env_name=='MountainCar-v0':
    bound=[[-1.2,-.07], [.6,.07]]

for i in range(n_feat):
    state_bins.append(pandas.cut([bound[0][i], bound[1][i]], bins=n_bins, retbins=True)[1][1:-1])
def state_to_bins(obs):
    obs_bin=[]
    for obsi in obs:
        obs_bin.append(to_bin(obsi,state_bins[i]))
    return build_state(obs_bin)

c_shp=0
#env = gym.wrappers.Monitor(env, 'recordings/cartpole-experiment-1', force=True)
if 0:
    with open('human_demo.json', 'r') as fp:
        tr=json.load(fp)
rewards={'env':[], 'shp':[] }
scores = []
if lambda_learning:
    ag = SarsaLambdaAgent(actions=list(range(env.action_space.n)))
else:
    ag = SarsaAgent(actions=list(range(env.action_space.n)))

for i_episode in range(1000):
    observation = env.reset()
    action = ag.choose_action(str(state_to_bins( observation)))
    if lambda_learning:
        ag.eligibility_trace *= 0
    t=0
    while True:
        # env.render()
        observation_, r_env, done, info = env.step(action)
        action_ = ag.choose_action(str(state_to_bins(observation_)))
        ag.learn(str(state_to_bins(observation)), action, r_env, str(state_to_bins(observation_)), action_)
        #r_shp = round(comp_shap_rew(observation, action, tr),2)
        #rewards['env'].append(r_env)
        #rewards['shp'].append(r_shp)

        observation,action = observation_,action_
        t+=1
        if done:
            print('episode:{}, score:{}'.format(i_episode,t))
            scores.append(int(t))
            if 0:
                with open('rewards.json', 'w') as fp:
                    json.dump(rewards, fp)
            break

with open('scores.json', 'w') as fp:
    json.dump(scores, fp)
plt.plot(scores)
plt.show()
print('scores={}'.format(scores))
env.close()