from envs.env_tree import *
import random

tree=[]
tree.append(node('2.1',5,[],r=2))
tree.append(node('2.2',8,[],r=3))
tree.append(node('1.1',6,[tree[0],tree[1]],r=4))
tree.append(node('root',0,[tree[2]],r=0))
env=tree_env(tree[-1])
s=env.reset()
i=0
score=0
while True:
    print('dist to next_junct={}'.format(env.s.dist))
    print('next_junct={}'.format(env.s.node.id))
    a=random.choice(env.get_actions(env.s))
    s_,r,done=env.step(a)
    score+=r
    if done:
        print('score={}'.format(score))
        break

    i+=1



