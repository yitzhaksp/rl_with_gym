import json
from utils.misc import *
import matplotlib.pyplot as plt
files=['scores.json','scores_shp.json']
lbs=['plain RL','shaping']
scrs_avgs_lst=[]
with open('scores_info.json', 'r') as fp:
    info=json.load(fp)

for file,lb in zip(files,lbs):
    with open(file, 'r') as fp:
        scrs=json.load(fp)
    scrs_avgs=running_mean(np.array(scrs),10)
    plt.plot(scrs_avgs,label=lb)
dist_to_goal_arr=info['dist_to_goal']*np.ones(scrs_avgs.size)
plt.plot(dist_to_goal_arr,label='dist_to_goal')
plt.legend()
#plt.legend((pl1,pl2),('plain RL','shaping'))
plt.show()
tmp=7