import gym
import time
env = gym.make('MountainCar-v0')
s = env.reset()
for t in range(1000):
    a = env.action_space.sample()
    s, r, done, info = env.step(a)
    #time.sleep(.5)
    if done:
        print("Episode finished after {} timesteps".format(t+1))
        break