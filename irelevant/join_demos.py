from utils.misc import *
import json

demos=['human_demo_1.json','human_demo_2.json']
hd_all={}
for demo in demos:
    with open(demo, 'r') as fp:
        x=json.load(fp)
        hd_all = join_dict_of_lst(hd_all,x) if any(hd_all.values()) else x

with open('human_demo_all.json', 'w') as fp:
    json.dump(hd_all, fp)

