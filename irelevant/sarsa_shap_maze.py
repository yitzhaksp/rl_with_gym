'''
Q-learning approach for different RL problems
as part of the basic series on reinforcement learning @
https://github.com/vmayoral/basic_reinforcement_learning
Inspired by https://gym.openai.com/evaluations/eval_kWknKOkPQ7izrixdhriurA
        @author: Victor Mayoral Vilches <victor@erlerobotics.com>
'''
import gym
import numpy
import random
import pandas
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain import *
from gym_maze.envs import MazeEnv
from gym_maze.envs.generators import *

maze = RandomBlockMazeGenerator(maze_size=8, obstacle_ratio=0.0)
init_and_goal_states={'i':[3,6],'g':[[8,4]]}
env = MazeEnv(maze,init_and_goal_states=init_and_goal_states)

n_feat = env.observation_space.shape[0]
lambda_learning=0

c_shp=0
with open('human_demo_all.json', 'r') as fp:
    tr=json.load(fp)
rewards={'env':[], 'shp':[] }
n_steps_list = []
if lambda_learning:
    ag = SarsaLambdaAgent(actions=list(range(env.action_space.n)))
else:
    ag = SarsaAgent(actions=list(range(env.action_space.n)))

for i_episode in range(200):
    observation = env.reset()
    action = ag.choose_action(str(observation))
    if lambda_learning:
        ag.eligibility_trace *= 0
    t=0
    while True:
        # env.render()
        observation_, r_env, done, info = env.step(action)
        action_ = ag.choose_action(str(observation_))
        r_shp = round(comp_shap_rew(env.state, str(action), tr),2)
        r=r_env+ c_shp*r_shp
        ag.learn(str(observation), action, r, str(observation_), action_)
        observation,action = observation_,action_
        t+=1
        if done:
            print('episode:{}, score:{}'.format(i_episode,t))
            n_steps_list.append(int(t))
            if 0:
                with open('rewards.json', 'w') as fp:
                    json.dump(rewards, fp)
            break

with open('scores_shp.json', 'w') as fp:
    json.dump(n_steps_list, fp)
info={'dist_to_goal': state_dist(init_and_goal_states['i'],init_and_goal_states['g'][0]) }
with open('scores_info.json', 'w') as fp:
    json.dump(info, fp)
plt.plot(n_steps_list)
plt.show()
print('scores={}'.format(n_steps_list))
env.close()