'''
SARSA with attention on random walk
'''
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain_general import *
from envs.env_tree import *

tree=[]
tree.append([])
tree[0].append(node('1.1', 50, [],r=2,dist_to_bump=[20]))
#tree[0].append(node('1.2', 50, [],r=1))
tree.append([])
tree[1].append(node('0', 0, tree[0]))

env=tree_env(tree)
k0=1
s0=state(0,tree[k0][0])
s_term=[]
for node_set in tree:
    for node in node_set:
        if not node.children:
            s_term.append(state(0,node))
to_str_funcs={
's_to_str':s_to,
'a_to_str':a_to
}
q_init=0
alpha=0.1
dbg=0
algs=[2,3]
n_sims=1
n_epochs=200
scores = np.zeros((len(algs),n_sims, n_epochs))
sim_info={
'alg_names':[None for i in algs]
}
for i_sim in range(n_sims):
    for i_alg,alg in enumerate(algs):
        np.random.seed(i_sim)
        if alg==1:
            ag = GenSarsaAgent(env.get_actions, q_init, s_term, to_str_funcs,alpha=alpha, dbg=dbg)
        elif alg==2:
            suf='lmb'
            lambda_=.9
            ag = GenSarsaLambdaAgent(env.get_actions, q_init, s_term, to_str_funcs, lambda_,alpha=alpha, dbg=dbg)
        elif alg==3:
            suf='avs'
            ag = Gen_nStepAgent_crit(env.get_actions,q_init,s_term,to_str_funcs,h,alpha=alpha,dbg=dbg)
        sim_info['alg_names'][i_alg]=suf
        s_obs = state(0, tree[k0][0])
        print(s_to(s_obs))
        a_obs = ag.get_actions(s_obs)[0]
        q_obs_opt = 2
        q_obs = []
        for i_ep in range(n_epochs):
            env.set_state(s0)
            s=s0
            a = ag.choose_action(s,ag.eps)
            r_cum,t=0,0
            while True:
                print(t)
                t+=1
                s_, r, done = env.step(a)
                r_cum+=r
                a_ = ag.choose_action(s_,ag.eps)
                ag.learn(s,a,r,s_,a_)
                s,a=s_,a_
                if done:
                    q_obs.append(ag.q_tab.loc[s_to(s_obs), a_to(a_obs)])
                    #print('q_obs=', q_obs)
                    print('sim:{}, episode:{}, score:{}'.format(i_sim,i_ep,r_cum))
                    scores[i_alg,i_sim,i_ep]=r_cum
                    break
        np.save('scores.npy',scores)
        with open('q_obs{}.json'.format(suf), 'w') as fp:
            json.dump(q_obs, fp)
with open('sim_info.json', 'w') as fp:
    json.dump(sim_info, fp)
#plt.plot(scores)
plt.plot(q_obs)
plt.plot(q_obs_opt*np.ones(len(q_obs)))
plt.show()
print('scores={}'.format(scores))
