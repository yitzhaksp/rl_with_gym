'''
SARSA with attention on random walk
'''
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain_elgtrace import *
from utils.RL_brain_atf import *

from envs.env_road import *

dbg=1
bumps=[2]
length=4
env=road_env(length,bumps)
k0=1
pos0=0
r_trh=-2.5
alpha=0.1
lambda_=.95
eps_decay=.99
algs=[3]
n_sims=20
n_epochs=200
scores = np.zeros((len(algs),n_sims, n_epochs))
q_init=0
sim_info={
'alg_names':[None]* len(algs)
}
to_str_funcs={
's_to_str':s_to,
'a_to_str':a_to,
'crit_to_str':c_to
}
pos_obs=bumps[0]+1
s_obs=state(pos_obs,bump=(pos_obs in bumps))
a_obs1,a_obs2=action('n'),action('c')
for i_sim in range(n_sims):
    for i_alg,alg in enumerate(algs):
        q_obs1, q_obs2 = [], []
        np.random.seed(i_sim)
        if alg==1:
            ag = QAgent_atf(env.get_actions, q_init, env.terminal, to_str_funcs,r_trh,alpha=alpha, dbg=dbg,eps_decay=eps_decay)
            suf='atf'
        elif alg==2:
            ag = QLambdaAgent(env.get_actions, q_init, env.terminal, to_str_funcs,lambda_,alpha=alpha, dbg=dbg,eps_decay=eps_decay)
            suf='lmd'
        elif alg==3:
            tmp=7
            ag = ElgTraceAgent_atf(env.get_actions, q_init, env.terminal, to_str_funcs,
                                  lambda_,r_trh,
                                  alpha=alpha, dbg=dbg,eps_decay=eps_decay)


        sim_info['alg_names'][i_alg]=suf
        for i_ep in range(n_epochs):
            env.set_state(pos0)
            s = copy.deepcopy(env.s)
            a = ag.choose_action(s,ag.eps)
            r_cum,t=0,0
            ag.eps*=ag.eps_decay
            while True:
                #print(t)
                t+=1
                s_, r, done = env.step(a)
                r_cum+=r
                ag.learn(s,a,r,s_)
                a_ = ag.choose_action(s_,ag.eps)
                s,a=s_,a_
                if done:
                    q_obs1.append(ag.q_tab.loc[s_to(s_obs), a_to(a_obs1)])
                    q_obs2.append(ag.q_tab.loc[s_to(s_obs), a_to(a_obs2)])
                    #print('q_obs=', q_obs)
                    print('sim:{}, episode:{}, score:{}'.format(i_sim,i_ep,r_cum))
                    scores[i_alg,i_sim,i_ep]=r_cum
                    break
    np.save('scores.npy',scores)

with open('sim_info.json', 'w') as fp:
    json.dump(sim_info, fp)
plt.plot(q_obs1,label=a_obs1.name)
plt.plot(q_obs2,label=a_obs2.name)
plt.legend()
plt.savefig('plot.png')
plt.show()
print('scores={}'.format(scores))
