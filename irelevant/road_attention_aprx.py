'''
SARSA with attention on random walk
'''
import json
from utils.misc import *
import matplotlib.pyplot as plt
from utils.RL_brain_approx import *
from utils.value_function import *
from envs.env_road import *
import copy

dbg=True
#bump_pos=2
bumps=[2]
length=4
env=road_env(length,bumps)
k0=1
pos0=0
#q_init=0
alpha=0.02
algs=[1]
SoQ='q'
n_sims=1
n_epochs=3000
scores = np.zeros((len(algs),n_sims, n_epochs))
sim_info={
'alg_names':[None for i in algs]
}
par={'ACTIONS':env.actions, 'EPSILON':.1, 'eps_decay':1.0,
     'gamma':1.0,'lamda':.9, 'render':0}
#w_opt=np.array([])
w0=np.random.randn(4)
valueFunc = ValueFunction(w0,alpha,par)
pos_obs=bumps[0]
s_obs=state(pos_obs,bump=(pos_obs in bumps))
a_obs1,a_obs2=action('n'),action('c')
for i_sim in range(n_sims):
    q_obs1,q_obs2=[],[]
    for i_alg,alg in enumerate(algs):
        #np.random.seed(i_sim)
        if alg==1:
            suf=SoQ
        sim_info['alg_names'][i_alg]=suf
        for i_ep in range(n_epochs):
            env.set_state(pos0)
            s0=copy.deepcopy(env.s)
            rcum=SarsaOrQLambda(s0, valueFunc,env.term_val, env, par, SoQ,dbg=dbg)
            par['EPSILON'] *= par['eps_decay']
            print('sim:{}, episode:{}, score:{}'.format(i_sim, i_ep, rcum))
            scores[i_alg, i_sim, i_ep] = rcum
            q_obs1.append(valueFunc.comp_value(s_obs,a_obs1))
            q_obs2.append(valueFunc.comp_value(s_obs,a_obs2))


    np.save('scores.npy',scores)
with open('sim_info.json', 'w') as fp:
    json.dump(sim_info, fp)
plt.plot(q_obs1,label=a_obs1.name)
plt.plot(q_obs2,label=a_obs2.name)
plt.legend()
plt.show()
print('scores={}'.format(scores))
