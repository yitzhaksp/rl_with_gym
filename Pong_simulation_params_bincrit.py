env='Pong'
crit_func='binary'
experiment_suf=''
logfiles_suf=crit_func+'_'+experiment_suf
sim_params={
'env':env,
'exploration_free_score':True,
'frames_per_sample':3,
'random_seed':1,
'cuda_device':'0',
'stop_at_ML':False,
'first_sim_index':0,
'render':False,
'resume':True,
'gamma':.99,
'opt_params':{"learning_rate": 0.0001},
'eps_start':1.0,
'eps_end' : .1,  # exploration
'eps_decay_time':5000,
'random_games':2000,
'learn_freq':200,
'history_batch':1000,
'replay_factor' : 20,
'save_freq':100,
'n_avg':100,
'e_fin':6800,
'crit_func': crit_func,
'model_type':'Tennis_small',
'model_architecture_file': 'models/'+env+'_qmodel_'+logfiles_suf+'.json',
'weights_file':  'models/'+env+'_qmodel_'+logfiles_suf+'_weights.h5',
}
sim_params['scores_log_path_base']='logs/scores_'+sim_params['env']+'_'+logfiles_suf
sim_params['variances_log_path_base']='logs/scores_'+sim_params['env']+'_'+logfiles_suf

sim_params['input_dim']=[80 , 80 , 3]
if sim_params['resume']:
    sim_params['n_simulations']=1
    sim_params['e_start']=3896
else:
    sim_params['n_simulations'] = 1
    sim_params['e_start'] = 0
