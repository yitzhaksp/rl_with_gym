import json
from utils.misc import *
import matplotlib.pyplot as plt
import os
score_file_inds=range(0,5)
filename_base='variance_based'
score_file_path_base='logs/Pong/'+filename_base+'_'
file_lengths=[]
for score_file_ind in score_file_inds:
    file_lengths.append(file_len(score_file_path_base + str(score_file_ind) + '.log'))
min_file_length=min(file_lengths)
print('minimal file length: {} '.format(min_file_length))


lines=[]
scrs_sum=np.zeros(min_file_length)
print('calculating average ...')
for score_file_ind in score_file_inds:
    score_file_path = score_file_path_base + str(score_file_ind) + '.log'
    with open(score_file_path,'r') as fp:
        scrs = fp.read().splitlines()
    scrs_num = [int(float(x)) for x in scrs]
    scrs_num=scrs_num[:min_file_length]
    scrs_sum+=np.array(scrs_num)
    #scrs_num=scrs_num[:15000]
scrs_avg=(1/len(score_file_inds))*scrs_sum
out_dir='processed_score_files'
if not os.path.exists(out_dir): os.makedirs(out_dir)
print('saving result ...')
np.save(  os.path.join(out_dir,filename_base+'.npy')  ,  scrs_avg )

tmp=7