<<<<<<< HEAD
from utils.value_function import *
from utils.RL_utils import *
from utils.RL_deep import *
from utils.pong_utils import *
import json
import gym
import numpy as np
import os
import time
from utils.keras_utils import *
import matplotlib.pyplot as plt
from collections import deque
from envs.env_Tennis import *
from misc_models import *

np.random.seed(1)
os.environ["CUDA_VISIBLE_DEVICES"]="1"

render=False
resume=False
input_dim = [80,80, 3]
gamma=.99
#update_frequency = 1
opt_params={"learning_rate": 0.0001}
#resume = False
eps_start=1.0
eps_end = .1  # exploration
eps_decay_time=10000
random_games=2000
learn_freq=200
history_batch = 1000
replay_factor = 100
start_learning = history_batch * replay_factor
max_history_len =  start_learning * 3
save_freq=100
n_avg=100
#learn_freq=5
dm=0 #dummy variable
crit_flg=False
if crit_flg:
    crit_func='critmodel'
    if crit_func=='critmodel':
        crit_model = load_model_composite('crit_model_Tennis.json', 'crit_model_Tennis_weights.h5')
else:
    crit_func='nocrit'


# Define environment/game
if  not os.path.isdir('logs'):
    os.mkdir('logs')
elapsed_time_log_path="logs/elapsed_time_"+crit_func+".log"

elapsed_time_log = open(elapsed_time_log_path, "w")
elapsed_time_log.close()
env = gym.make('Pong-v0')
actions=range(env.action_space.n)
n_act = env.action_space.n  # This is incorrect for Pong (?)
model_type=1
model_architecture_file = 'Pong_qmodel_'+crit_func+'.json'
weights_file = 'Pong_qmodel_'+crit_func+'_weights.h5'
if resume:
    e_start=2300
    n_simulations = 1
    model_main = load_model_composite(model_architecture_file, weights_file)
    model_trg = load_model_composite(model_architecture_file, weights_file)
    model_main.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))
else:
    e_start=0
    n_simulations = 10
e_fin=20000
for sim in range(n_simulations):
    scores_log_path="logs/scores_Pong_crit_"+crit_func+'_'+str(sim)+".log"
    scores_log = open(scores_log_path, "w")
    scores_log.close()
    if not resume:
        model_main =Pong_model(input_dim,n_act,model_type,opt_params)
        model_trg =Pong_model(input_dim,n_act,model_type,opt_params)
        with open(model_architecture_file, "w") as outfile:
            outfile.write(model_main.to_json())
    value_func_main=ValueFunction_nn(model_main,{})
    value_func_trg=ValueFunction_nn(model_trg,{})

    exp_repl_par={'gamma':gamma,
                  'len_memory':max_history_len}
    exp_replay=experience_replay(exp_repl_par)
    e=0 #epoch
    curr_frame=0
    tot_updates=0
    recent_scores=deque()
    start_time = time.time()
    for e in range(e_start,e_fin):
        if e%10==0:
            print('elapsed time: {} '.format(time.time()-start_time))
            elapsed_time_log = open(elapsed_time_log_path, "a")
            elapsed_time_log.write(str(e) +', ' +str(time.time()-start_time)+'\n')
            elapsed_time_log.close()
        eps=get_epsilon(e, random_games, eps_start, eps_end, eps_decay_time)
        s_prev, x_prev=None,None
        s=env.reset()
        s_prc=preprocess_screen(s)
        x = np.stack([s_prc,s_prc,s_prc],2)
        action_values = value_func_main.comp_value(x)
        a=choose_action(actions,action_values , eps)
        done = False
        i=0
        score=0
        while not done:
            if x_prev is not None:
                x = np.stack([x_prev[:,:,1],x_prev[:,:,2], preprocess_screen(s)], 2)
            action_values=value_func_main.comp_value(x)
            a = choose_action(actions, action_values, eps)
            s_nxt, r, done,info = env.step(a)
            score+=r
            x_nxt = np.stack([x[:,:,1],x[:,:,2], preprocess_screen(s_nxt)], 2)
            action_values_nxt=value_func_main.comp_value(x_nxt)
            a_nxt_best = choose_action(actions,action_values_nxt,0.0)
            action_values_nxt_trg=value_func_trg.comp_value(x_nxt)
            q_nxt_best=action_values_nxt_trg[a_nxt_best]
            if crit_flg:
                if crit_func=='binary':
                    crit = comp_crit(s, s_prev)
                elif crit_func=='linear':
                    crit = comp_crit_lin(s, s_prev, field_length)
                elif crit_func == 'critmodel':
                    crit = crit_model.predict(np.expand_dims(x, 0))
                exp_replay.update_atn(x, r, a, done, q_nxt_best, crit)
            else:
                exp_replay.update(x, r, a, done, gamma,q_nxt_best)
            if i%learn_freq==0 and len(exp_replay.ready_for_upd) > start_learning:
                xs,aas,rcums,done_nxts,disc_fcts,q_trgs=exp_replay.get_batch(history_batch)
                ys=[]
                for xx,aa,rcum,done_nxt,disc_fct,q_trg in zip(xs,aas,rcums,done_nxts,disc_fcts,q_trgs):
                    trg=comp_trg(rcum,disc_fct,q_trg,done_nxt)
                    y=value_func_main.comp_value(xx)
                    # Thou shalt not correct actions not taken #deep
                    y[aa]=trg
                    ys.append(y)
                if ys: #list not empty?
                    ys=np.vstack(ys)
                    print('model update nr. {}'.format(tot_updates))
                    value_func_main.learn(xs,ys)
                    tot_updates+=1
                if tot_updates % save_freq == 0:
                    print('saving model ...')
                    model_main.save_weights(weights_file, overwrite=True)
                    model_trg.set_weights(model_main.get_weights())
            s_prev=s
            s = s_nxt
            x_prev=x
            i+=1

        recent_scores.append(score)
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores) < n_avg:
            running_mean = sum(recent_scores) / len(recent_scores)
        else:  # more efficient computation of running average
            running_mean += (recent_scores[-1] - recent_scores[0]) / n_avg
        print("Pong |criticality {}|simulation {} |Epoch {} | running mean {}".format(crit_func,sim,e, running_mean))
        #print('best wincount = {}'.format(win_cnt_best))
        scores_log = open(scores_log_path, "a")
        scores_log.write(str(score) + "\n")
        scores_log.close()

tmp=7
>>>>>>> 02e6b35673f1e1ffc74e975bad72f92d7f39cff8
=======
from utils.value_function import *
from utils.RL_utils import *
from utils.RL_deep import *
from utils.pong_utils import *
import json
import gym
import numpy as np
import os
import time
from utils.keras_utils import *
import matplotlib.pyplot as plt
from collections import deque
from misc_models import *
from Pong_simulation_params import simulation_params


random_seed= simulation_params['random_seed']
cuda_device=simulation_params['cuda_device']
input_dim=simulation_params['input_dim']
render=simulation_params['render']
resume=simulation_params['resume']
gamma=simulation_params['gamma']
opt_params=simulation_params['opt_params']
eps_start=simulation_params['eps_start']
eps_end=simulation_params['eps_end']  # exploration
eps_decay_time=simulation_params['eps_decay_time']
random_games=simulation_params['random_games']
learn_freq=simulation_params['learn_freq']
history_batch=simulation_params['history_batch']
replay_factor=simulation_params['replay_factor']
save_freq=simulation_params['save_freq']
n_avg=simulation_params['n_avg']
crit_func=simulation_params['crit_func']
model_type=simulation_params['model_type']
model_architecture_file=simulation_params['model_architecture_file']
weights_file=simulation_params['weights_file']
n_simulations=simulation_params['n_simulations']
e_start=simulation_params['e_start']
e_fin=simulation_params['e_fin']


np.random.seed(random_seed)
os.environ["CUDA_VISIBLE_DEVICES"]=cuda_device
start_learning = history_batch * replay_factor
max_history_len =  start_learning * 3
np.random.seed(random_seed)
os.environ["CUDA_VISIBLE_DEVICES"]=cuda_device

if crit_func=='critmodel':
    crit_model = load_model_composite('crit_model_Tennis.json', 'crit_model_Tennis_weights.h5')

# Define environment/game
if  not os.path.isdir('logs'):
    os.mkdir('logs')
elapsed_time_log_path="logs/elapsed_time_"+crit_func+".log"

elapsed_time_log = open(elapsed_time_log_path, "w")
elapsed_time_log.close()
env = gym.make('Pong-v0')
actions=range(env.action_space.n)
n_act = env.action_space.n  # This is incorrect for Pong (?)

model_updated=False
if resume:
    model_main = load_model_composite(model_architecture_file, weights_file)
    model_trg = load_model_composite(model_architecture_file, weights_file)
    model_main.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))

for sim in range(n_simulations):
    scores_log_path="logs/scores_Pong_crit_"+crit_func+'_'+str(sim)+".log"
    scores_log = open(scores_log_path, "w")
    scores_log.close()
    if not resume:
        model_main =Pong_model(input_dim,n_act,model_type,opt_params)
        model_trg =Pong_model(input_dim,n_act,model_type,opt_params)
        with open(model_architecture_file, "w") as outfile:
            outfile.write(model_main.to_json())
    value_func_main=ValueFunction_nn(model_main,{})
    value_func_trg=ValueFunction_nn(model_trg,{})

    exp_repl_par={'gamma':gamma,
                  'len_memory':max_history_len}
    exp_replay=experience_replay(exp_repl_par)
    e=0 #epoch
    curr_frame=0
    tot_updates=0
    recent_scores=deque()
    start_time = time.time()
    for e in range(e_start,e_fin):
        if e%10==0:
            print('elapsed time: {} '.format(time.time()-start_time))
            elapsed_time_log = open(elapsed_time_log_path, "a")
            elapsed_time_log.write(str(e) +', ' +str(time.time()-start_time)+'\n')
            elapsed_time_log.close()
        eps=get_epsilon(e, random_games, eps_start, eps_end, eps_decay_time)
        s_prev, x_prev=None,None
        s=env.reset()
        s_prc=preprocess_screen(s)
        x = np.stack([s_prc,s_prc,s_prc],2)
        action_values = value_func_main.comp_value(x)
        a=choose_action(actions,action_values , eps)
        done = False
        i=0
        score=0
        while not done:
            if x_prev is not None:
                x = np.stack([x_prev[:,:,1],x_prev[:,:,2], preprocess_screen(s)], 2)
            action_values=value_func_main.comp_value(x)
            a = choose_action(actions, action_values, eps)
            s_nxt, r, done,info = env.step(a)
            score+=r
            x_nxt = np.stack([x[:,:,1],x[:,:,2], preprocess_screen(s_nxt)], 2)
            action_values_nxt=value_func_main.comp_value(x_nxt)
            a_nxt_best = choose_action(actions,action_values_nxt,0.0)
            action_values_nxt_trg=value_func_trg.comp_value(x_nxt)
            q_nxt_best=action_values_nxt_trg[a_nxt_best]
            if crit_func=='nocrit':
                pass
            elif crit_func=='binary':
                crit = comp_crit(s, s_prev)
            elif crit_func=='linear':
                crit = comp_crit_lin(s, s_prev)
            elif crit_func == 'critmodel':
                crit = crit_model.predict(np.expand_dims(x, 0))
            else:
                raise Exception('criticality function '+crit_func+" doesn't exist")

            if crit_func=='nocrit':
                exp_replay.update(x, r, a, done, gamma, q_nxt_best)
            else:
                exp_replay.update_atn(x, r, a, done, q_nxt_best, crit)
            if i%learn_freq==0 and len(exp_replay.ready_for_upd) > start_learning:
                xs,aas,rcums,done_nxts,disc_fcts,q_trgs=exp_replay.get_batch(history_batch)
                ys=[]
                for xx,aa,rcum,done_nxt,disc_fct,q_trg in zip(xs,aas,rcums,done_nxts,disc_fcts,q_trgs):
                    trg=comp_trg(rcum,disc_fct,q_trg,done_nxt)
                    y=value_func_main.comp_value(xx)
                    # Thou shalt not correct actions not taken #deep
                    y[aa]=trg
                    ys.append(y)
                if ys: #list not empty?
                    ys=np.vstack(ys)
                    value_func_main.learn(xs,ys)
                    tot_updates+=1
                    print('model update nr. {}'.format(tot_updates))
                    model_updated=True
                if tot_updates % save_freq == 0:
                    print('saving model ...')
                    model_main.save_weights(weights_file, overwrite=True)
                    model_trg.set_weights(model_main.get_weights())
            s_prev=s
            s = s_nxt
            x_prev=x
            i+=1

        recent_scores.append(score)
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores) < n_avg:
            running_mean = sum(recent_scores) / len(recent_scores)
        else:  # more efficient computation of running average
            running_mean += (recent_scores[-1] - recent_scores[0]) / n_avg
        print("Pong |criticality {}|simulation {} |Epoch {} | running mean {}".format(crit_func,sim,e, running_mean))
        #print('best wincount = {}'.format(win_cnt_best))
        if resume:
            if model_updated:
                scores_log = open(scores_log_path, "a")
                scores_log.write(str(score) + "\n")
                scores_log.close()
        else:
            scores_log = open(scores_log_path, "a")
            scores_log.write(str(score) + "\n")
            scores_log.close()
tmp=7
>>>>>>> 7b6cbfb18c269260a92c8c5616a02d2db9e6d92e
