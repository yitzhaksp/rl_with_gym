from envs.env_Tennis import *
from utils.RL_brain_crit import *
from collections import deque
import time
import numpy as np

env=simple_pong_env(16,8)
def get_actions(s):
    return range(env.action_space.n)
def x_to_str(x):
    return 'prev_'+ s_to_str(x[0])+'_current_'+ s_to_str(x[1])
def a_to_str(a):
    return str(a)
# attention function
# remember that x=[s_prev,s]
def h(x):
    if x[1].velocity[1]==1:
        return 1
    else:
        return 0

to_str_funcs={
's_to_str':x_to_str,
'a_to_str':a_to_str
}

np.random.seed(1)
scores_log = open("logs/scores_atn.log", "w")
n_avg=100
recent_scores=deque()
eps=.1
points_to_play=30000
q_init=0
ag = Gen_nStepAgent_crit(get_actions, q_init, to_str_funcs,h,eps=eps)
for point in range(points_to_play):
    s_prev=None
    s = env.reset()
    x = [s_prev, s]
    ag.add_state_to_qtab(x)
    score=0
    while True:
        x=[s_prev,s]
        a = ag.choose_action_from_qtab(x,eps)
        s_, r, done = env.step(a)
        score+=r
        x_=[s,s_]
        ag.add_state_to_qtab(x_)
        ag.learn(x,a,r,x_,done)
        s_prev=s
        s=s_
        if done:
            recent_scores.append(score)
            if len(recent_scores) >n_avg:
                recent_scores.popleft()
            if len(recent_scores) <n_avg:
                running_mean = sum(recent_scores) / len(recent_scores)
            else: #more efficient computation of running average
                running_mean+= (recent_scores[-1]-recent_scores[0])/n_avg
            if point%100==0:
                print('point nr: {}, running mean: {:.2}'.format(point, running_mean))
            scores_log.write(str(score) + "\n")

            break
        #time.sleep(1.0)

tmp=7