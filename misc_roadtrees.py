from envs.env_tree import *

def get_tree(tree_num):
    tree=[]
    if tree_num==1:
        tree.append(node('2.1', 15, [], r=1))
        tree.append(node('2.2', 15, [], r=2))
        tree.append(node('2.3', 10, [], r=0))
        tree.append(node('2.4', 15, [], r=7))
        tree.append(node('1.1', 20, [tree[0], tree[1]], r=1))
        tree.append(node('1.2', 10, [tree[2], tree[3]], r=0))
        tree.append(node('root', 0, [tree[-1], tree[-2]], r=0))
    elif tree_num==2:
        tree.append(node('1.1', 50, [], r=1))
        tree.append(node('1.2', 50, [], r=2))
        tree.append(node('root', 0, [tree[-1], tree[-2]], r=0))
    elif tree_num==3:
        bad_final_nodes=[]
        n_bad_final_nodes=19
        for i in range(n_bad_final_nodes):
            bad_final_nodes.append(node('2.2.'+str(i+1), 10, [], r=-2))
        final_nodes_right_side=bad_final_nodes+[ node(str(n_bad_final_nodes+1),10,[],r=1) ]
        final_nodes_left_side=[node('2.1.1', 10, [], r=0),node('2.1.2', 10, [], r=1)]
        tree.append(node('1.1', 10, final_nodes_left_side, r=0))
        tree.append(node('1.2', 10, final_nodes_right_side, r=1))
        tree.append(node('root', 0, [tree[-1], tree[-2]], r=0))
    return tree

#env=tree_env(tree_1[-1])