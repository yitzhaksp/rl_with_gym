# 12aug
def step(self, a):
    assert (self.s.ball[0] >= 0 and self.s.ball[0] < self.width)
    assert (self.s.ball[1] >= 0 and self.s.ball[1] < self.length)
    r = 0
    done = False
    if self.s.ball[1] == 1 or (self.s.ball[1] == (self.length - 2)):  # ball at racketcolumn
        if self.s.ball[0] == 0 or (self.s.ball[1] == self.s.width - 1):  # ball at wall
            self.s.velocity[0] *= -1
        if self.s.ball[1] == 1:  # ball at oponentcolumn
            if self.s.ball[0] == self.s.oponnent:  # ball hits racket
                self.s.velocity[1] = 1
            elif self.s.velocity[1] == -1:
                self.s.ball[1] = 0
                r = 1
                done = True
        elif self.s.ball[1] == (self.length - 2):  # ball at agentcolumn
            if self.s.ball[0] == self.s.me:  # ball hits racket
                self.s.velocity[1] = -1
            else:
                self.s.ball[1] = self.length - 1
                r = -1
                done = True

    elif self.s.ball[0] == 0 or (self.s.ball[1] == self.s.width - 1):  # ball at wall
        self.s.ball[1] += self.s.velocity[1]
        self.s.velocity[0] *= -1
    else:
        self.s.ball += self.s.velocity
    return copy.deepcopy(self.s), r, done
#1may
if ag.s_to(s_obs) in ag.q_tab.index:
    if a_to(a_obs) in ag.q_tab[s_to(s_obs)].index:
        tmp = 7
        # print('q_obs='+str(ag.q_tab.loc[s_to(s_obs), a_to(a_obs)]))