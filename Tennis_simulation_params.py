crit_func='variance_based'
sim_params={
'env':'Tennis',
'frames_per_sample':3,
'random_seed':1,
'cuda_device':'1',
'field_width':20,
'field_length':40,
'exploration_free_score':False,
'render':False,
'resume':False,
'gamma':.99,
'opt_params':{"learning_rate": 0.0001},
'eps_start':1.0,
'eps_end' : .1,  # exploration
'eps_decay_time':10000,
'random_games':2000,
'learn_freq':200,
'history_batch':1000,
'replay_factor' : 1,
'save_freq':100,
'n_simulations': 10,
'n_avg':100,
'first_sim_index':0,
'e_fin':70000,
'crit_func': crit_func,
'randcrit_type':'mixed',
'model_type':'Tennis_small',
'model_architecture_file': 'models/Tennis_qmodel_'+crit_func+'.json',
'weights_file':  'models/Tennis_qmodel_'+crit_func+'_weights.h5',
}
sim_params['scores_log_path_base']='logs/scores_'+sim_params['env']+'_'+sim_params['crit_func']
sim_params['input_dim']=[sim_params['field_width'] , sim_params['field_length'] , 3]
if sim_params['resume']:
    sim_params['e_start']=5100
else:
    sim_params['e_start'] = 0